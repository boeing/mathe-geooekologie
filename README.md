# Kurs: Geoökologische Anwendung quantitativer Methoden

Übungen für den Kurs "Geoökologische Anwendung quantitativer Methoden"

Stand Sommersemester 2023.

Über diesen Binder könnt ihr auf die Jupyter Notebooks zugreifen und diese bearbeiten. Allerdings ist dort kein Speichern möglich.

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgit.ufz.de%2Fboeing%2Fmathe-geooekologie/main)
